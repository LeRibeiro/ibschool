
package br.com.ischool.web.command.impl;

import br.com.ischool.core.aplicacao.Resultado;
import br.com.ischool.dominio.EntidadeDominio;

public class AlterarCommand extends AbstractCommand{

	
	public Resultado execute(EntidadeDominio entidade) {
		
		return fachada.alterar(entidade);
	}

}
