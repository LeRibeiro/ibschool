
package br.com.ischool.web.command.impl;

import br.com.ischool.core.fachada.IFachada;
import br.com.ischool.core.fachada.impl.Fachada;
import br.com.ischool.web.command.ICommand;



public abstract class AbstractCommand implements ICommand {

	protected IFachada fachada = new Fachada();

}
