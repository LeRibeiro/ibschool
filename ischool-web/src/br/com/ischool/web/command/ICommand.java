
package br.com.ischool.web.command;

import br.com.ischool.core.aplicacao.Resultado;
import br.com.ischool.dominio.EntidadeDominio;

public interface ICommand {

	public Resultado execute(EntidadeDominio entidade);
	
}
