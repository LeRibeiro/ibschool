
package br.com.ischool.web.vh.impl;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.ischool.core.aplicacao.Resultado;
import br.com.ischool.core.util.ConverteDate;
import br.com.ischool.dominio.Aluno;
import br.com.ischool.dominio.Cidade;
import br.com.ischool.dominio.DDD;
import br.com.ischool.dominio.Endereco;
import br.com.ischool.dominio.EntidadeDominio;
import br.com.ischool.dominio.Estado;
import br.com.ischool.dominio.Operadora;
import br.com.ischool.dominio.Pais;
import br.com.ischool.dominio.Telefone;
import br.com.ischool.dominio.Tipo;
import br.com.ischool.web.vh.IViewHelper;


public class AlunoViewHelper implements IViewHelper {

	public EntidadeDominio getEntidade(HttpServletRequest request) {
		String operacao = request.getParameter("operacao");
		Aluno aluno = null; 
		Endereco endereco = null;
		Pais pais = null;
		Estado estado = null;
		Cidade cidade = null;
		Telefone tel = null;
		Tipo tipo = null;
		Operadora op = null;
		DDD ddd = null;
		List<Telefone> tels = new ArrayList<>();
		
		
		if(operacao.equals("SALVAR")){
			aluno = new Aluno();
			endereco = new Endereco();
			cidade = new Cidade();
			estado = new Estado();
			pais = new Pais();
			tel = new Telefone();
			tipo = new Tipo();
			op = new Operadora();
			ddd = new DDD();
			
			// Aluno
			String nome = request.getParameter("nome");
			String nascimento = request.getParameter("nascimento");
			String cpf = request.getParameter("cpf");
			String rg = request.getParameter("rg");
			String email = request.getParameter("email");
			// Telefone
			String telefone = request.getParameter("telefone");
			String txtDDD = request.getParameter("ddd");
			String txtTipo = request.getParameter("tipo");
			String operadora_desc = request.getParameter("operadora_desc");
			String operadora_num = request.getParameter("operadora_num");
			// Endere�o
			String logradouro = request.getParameter("logradouro");
			String cep = request.getParameter("cep");
			String numero = request.getParameter("numero");
			String complemento = request.getParameter("complemento");
			String bairro = request.getParameter("bairro");
			String txtCidade = request.getParameter("cidade");
			String txtEstado = request.getParameter("estado");
			String txtPais = request.getParameter("pais");
			
			
			pais.setNome(txtPais);
			estado.setPais(pais);
			estado.setNome(txtEstado);
			cidade.setEstado(estado);
			cidade.setNome(txtCidade);
			endereco.setCidade(cidade);
			endereco.setLogradouro(logradouro);
			endereco.setCep(cep);
			endereco.setNumero(Integer.parseInt(numero));
			endereco.setComplemento(complemento);
			endereco.setBairro(bairro);
			aluno.setEndereco(endereco);
			aluno.setNome(nome);
			aluno.setCpf(cpf);
			aluno.setRg(rg);
			aluno.setEmail(email);
			
			tel.setNumero(telefone);
			tipo.setDescricao(txtTipo);
			tel.setTipo(tipo);
			op.setDescricao(operadora_desc);
			op.setNumero(Integer.parseInt(operadora_num));
			tel.setOperadora(op);
			ddd.setNumero(Integer.parseInt(txtDDD));
			tel.setDdd(ddd);
			
			tels.add(tel);
			aluno.setTelefones(tels);
			
			
			
			String formatoDt = "dd-MM-yyyy";
			// Formatar a data que receber do cliente - FORMATADOR DA DATA!!!!
			SimpleDateFormat df = new SimpleDateFormat(formatoDt);
			
			try {
				Date dataNascimento = df.parse(nascimento);
				aluno.setDtNascimento(dataNascimento);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			
			
			
			System.out.println("TESTE DA TELA:\n"+
			"Nome: " + nome +
			"\nNascimento: " + nascimento +
			"\nCPF: " + cpf +
			"\nRG: " + rg +
			"\nEmail: " + email +
			"\nTelefone: " + telefone +
			"\nDDD: " + ddd +
			"\nTipo: " + tipo +
			"\nDescri��o Operadora: " + operadora_desc +
			"\nNumero Operadora: " + operadora_num +
			"\nRua: " + logradouro +
			"\nCEP: " + cep +
			"\nNumero: " + numero +
			"\nComplemento: " + complemento +
			"\nBairro: " + bairro +
			"\nCidade: " + cidade.getNome() +
			"\nEstado: " + estado.getNome() +
			"\nPais: " + pais.getNome());

		}		
		else if(operacao.equals("VISUALIZAR")){
			
			
			String nome = request.getParameter("nome");
			String nascimento = request.getParameter("nascimento");
			String cpf = request.getParameter("cpf");
			String rg = request.getParameter("rg");
			String email = request.getParameter("email");
			// Telefone
			String telefone = request.getParameter("telefone");
			String txtDDD = request.getParameter("ddd");
			String txtTipo = request.getParameter("tipo");
			String operadora_desc = request.getParameter("operadora_desc");
			String operadora_num = request.getParameter("operadora_num");
			// Endere�o
			String logradouro = request.getParameter("logradouro");
			String cep = request.getParameter("cep");
			String numero = request.getParameter("numero");
			String complemento = request.getParameter("complemento");
			String bairro = request.getParameter("bairro");
			String txtCidade = request.getParameter("cidade");
			String txtEstado = request.getParameter("estado");
			String txtPais = request.getParameter("pais");
		
			
			aluno = new Aluno();
			
			
			
			if(nome != null && !nome.trim().equals("")){
				aluno.setNome(nome);
			}
			
			if(cpf != null && !cpf.trim().equals("")){
				aluno.setCpf(cpf);
			}
			
			if(rg != null && !rg.trim().equals("")){
				aluno.setRg(rg);
			}
			
			if(email != null && !email.trim().equals("")){
				aluno.setEmail(email);;
			}
			
			if(nascimento != null && !nascimento.trim().equals("")){
				aluno.setDtNascimento(ConverteDate.converteStringDate(nascimento));
			}
			
		}else if(operacao.equals("CONSULTAR")){
			aluno = new Aluno();
		}
		
		else{
			HttpSession session = request.getSession();
			Resultado resultado = (Resultado) session.getAttribute("resultado");
			String txtId = request.getParameter("txtId");
			int id = 0;
			
			if(txtId != null && !txtId.trim().equals("")){
				id = Integer.valueOf(txtId);
			}
			
			if(resultado != null){
				for(EntidadeDominio e: resultado.getEntidades()){
					if(e.getId() == id){
						aluno = (Aluno)e;
					}
				}
			}			
		}
		
		//retornar o objeto para a servlet
		return aluno;
			
	}
	
	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		
		RequestDispatcher d = null;
		String operacao = request.getParameter("operacao");
		
		if(resultado.getMsg() == null){
			if(operacao.equals("SALVAR")){
				resultado.setMsg("Aluno cadastrado com sucesso!  vh");
				
				request.setAttribute("resultado", resultado);
				d = request.getRequestDispatcher("FormConsultaAluno.jsp");	
			}
		}
		
		if(resultado.getMsg() == null && operacao.equals("CONSULTAR")){
			
			request.setAttribute("resultado", resultado);
			d = request.getRequestDispatcher("FormConsultaAluno.jsp");
		}
		
		if(resultado.getMsg() == null && operacao.equals("ALTERAR")){
			
//			request.setAttribute("aluno", resultado.getEntidades().get(0));
			d = request.getRequestDispatcher("FormConsultaAluno.jsp");  
		}
		
		if(resultado.getMsg() == null && operacao.equals("VISUALIZAR")){
			
			request.setAttribute("aluno", resultado.getEntidades().get(0));
			d = request.getRequestDispatcher("FormAluno.jsp");  			
		}
		
		if(resultado.getMsg() == null && operacao.equals("EXCLUIR")){
			
			request.getSession().setAttribute("resultado", null);
			d = request.getRequestDispatcher("FormConsultaAluno.jsp");  
		}
		
		if(resultado.getMsg() != null){
			if(operacao.equals("SALVAR") || operacao.equals("ALTERAR")){
				request.getSession().setAttribute("resultado", resultado);
				d = request.getRequestDispatcher("FormConsultaAluno.jsp");  	
			}
		}
		d.forward(request, response); 
	}

}
