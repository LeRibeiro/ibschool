<%@page import="br.com.ischool.core.util.ConverteDate"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page
	import="br.com.ischool.core.aplicacao.Resultado, br.com.ischool.dominio.*, java.util.*"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- @author Leticia Ribeiro -->
<html>
<head>
<title>:::: CONSULTAR ALUNO::::</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

</head>
<body>

			<%
				Resultado resultado = (Resultado) request.getAttribute("resultado");
			%>

			<form action="/ischool-web/SalvarAluno" method="post">

				<label for="txtId">ID:</label> 
				<input type="text" id="txtId" name="txtId" /> 
				<br> 
				<label for="txtNome">NOME:</label> 
				<input type="text" id="txtNome" name="txtNome" /> 
				<br>
				<label for="txtCpf">CPF:</label> 
				<input type="text" id="txtCpf" name="txtCpf" /> 
				<input type="submit" id="operacao" name="operacao" value="CONSULTAR" />
			</form>

			<%
				if (resultado != null && resultado.getMsg() != null) {
					out.print(resultado.getMsg());
				}
			%>
			<BR>

			<TABLE BORDER="5" WIDTH="50%" CELLPADDING="4" CELLSPACING="3">
				<TR>
					<TH COLSPAN="5"><BR>
						<H3>ALUNOS</H3></TH>
				</TR>

				<TR>
					<TH>ID</TH>
					<TH>Nome</TH>
					<TH>Data Nascimento</TH>
					<TH>CPF</TH>
					<TH>Email</TH>
				</TR>


				<%
					if (resultado != null) {
						List<EntidadeDominio> entidades = resultado.getEntidades();
						StringBuilder sbRegistro = new StringBuilder();
						StringBuilder sbLink = new StringBuilder();

						if (entidades != null) {
							for (int i = 0; i < entidades.size(); i++) {
								Aluno a = (Aluno) entidades.get(i);
								sbRegistro.setLength(0);
								sbLink.setLength(0);

								//	<a href="nome-do-lugar-a-ser-levado">descri��o</a>

								sbRegistro.append("<TR ALIGN='CENTER'>");

								sbLink.append("<a href=SalvarAluno?");
								sbLink.append("txtId=");
								sbLink.append(a.getId());
								sbLink.append("&");
								sbLink.append("operacao=");
								sbLink.append("VISUALIZAR");

								sbLink.append(">");

								sbRegistro.append("<TD>");
								sbRegistro.append(sbLink.toString());
								sbRegistro.append(a.getId());
								sbRegistro.append("</a>");
								sbRegistro.append("</TD>");

								sbRegistro.append("<TD>");
								sbRegistro.append(sbLink.toString());
								sbRegistro.append(a.getNome());
								sbRegistro.append("</a>");
								sbRegistro.append("</TD>");

								sbRegistro.append("<TD>");
								sbRegistro.append(sbLink.toString());
								sbRegistro.append(a.getDtNascimento());
								sbRegistro.append("</a>");
								sbRegistro.append("</TD>");

								sbRegistro.append("<TD>");
								sbRegistro.append(sbLink.toString());
								sbRegistro.append(a.getCpf());
								sbRegistro.append("</a>");
								sbRegistro.append("</TD>");
								
								sbRegistro.append("<TD>");
								sbRegistro.append(sbLink.toString());
								sbRegistro.append(a.getEmail());
								sbRegistro.append("</a>");
								sbRegistro.append("</TD>");
								
								sbRegistro.append("</TR>");

								out.print(sbRegistro.toString());

							}
						}
					}
				%>


			</TABLE>
</body>
</html>
