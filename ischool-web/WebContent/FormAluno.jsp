<%@ page import="br.com.ischool.core.aplicacao.Resultado"%> 
<%@ page import="br.com.ischool.dominio.*, java.util.* "%>
<%@ page import="br.com.ischool.core.util.ConverteDate"%>
	
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>:::: CADASTRO DE ALUNO::::</title>
</head>
<body>

	<%
		Aluno aluno = (Aluno) request.getSession().getAttribute("aluno");
	%>

		<form action="/ischool-web/SalvarAluno" method="post">
		
		<input type="hidden" id="txtId" name="txtId" value=
			<%
				if(aluno != null) out.print("'"+aluno.getId()+"' readonly>");
				else out.print(">");
			%>
		</input>
		
		
		<label for="nome">Nome:</label>
		<input type="text" id="nome" name="nome" value=
			<%
				if(aluno != null) out.print("'"+aluno.getNome()+"' readonly>");
				else out.print(">");
			%>
		</input>
		</br> 
		<label for="nascimento">Data de Nascimento:</label>
		<input type="text" id="nascimento" name="nascimento" value=
			<%
				if(aluno != null) out.print("'"+ConverteDate.converteDateString(aluno.getDtNascimento())+"' readonly>");
				else out.print(">");
			%>
		</input>
		<br>
		<label for="cpf">CPF:</label>
		<input type="text" id="cpf" name="cpf" value=
			<%
				if(aluno != null) out.print("'"+aluno.getCpf()+"'>");
				else out.print(">");
			%>
		</input>
		<br>
		<label for="rg">RG:</label>
		<input type="text" id="rg" name="rg" value=
			<%
				if(aluno != null) out.print("'"+aluno.getRg()+"'>");
				else out.print(">");
			%>
		</input>
		<br>
		<label for="rg">Email:</label>
		<input type="text" id="email" name="email" value=
			<%
				if(aluno != null) out.print("'"+aluno.getEmail()+"'>");
				else out.print(">");
			%>
		</input>
		<br>
		<label for="telefone">Telefone:</label>
		<input type="text" id="telefone" name="telefone" value=
			<%
				if(aluno != null){
				 	// se existir um aluno para exibir
					for(int i = 0; i < aluno.getTelefones().size(); i++){ 
						// para cada telefone, iremos criar um input
				  		// cada input de telefone ter� o id telefone_X. Sendo X o id do telefone.
				  		// tambem conter� o numero do telefone no atributo value do input
				  		out.print("'"+aluno.getTelefones().get(i).getNumero()+"'/>");
				 	}
				}
				else{ // n�o existe um aluno para exibir. � um novo cadastro
				 out.print(">");
				}
			%>
		</input>
		
		<label for="ddd">DDD:</label>
		<input type="text" id="ddd" name="ddd" value=
			<%
				if(aluno != null){
					for(int i = 0; i < aluno.getTelefones().size(); i++){ 
				  		out.print("'"+aluno.getTelefones().get(i).getDdd().getNumero()+"'/>");
				 	}
				} else{
					 out.print(">");
				}		
			%>
		</input>
		
		<label for="tipo">Tipo:</label>
		<input type="text" id="tipo" name="tipo" value=
			<%
 				if(aluno != null) {
 					for(int i = 0; i < aluno.getTelefones().size(); i++){
 						out.print("'"+aluno.getTelefones().get(i).getTipo().getDescricao()+"'/>");	
 					}
 				} else {
 					out.print(">");
 				}
 			%> 
		</input>
		
		<label for="operadora_desc">Nome Operadora:</label>
		<input type="text" id="operadora_desc" name="operadora_desc" value=
			<%
				if(aluno != null) {
					for(int i = 0; i < aluno.getTelefones().size(); i++){
						out.print("'"+aluno.getTelefones().get(i).getOperadora().getDescricao()+"'/>");	
					}
				} else {
					out.print(">");
				}
			%>
		</input>
		
		<label for="operadora_num">Numero Operadora:</label>
		<input type="text" id="operadora_num" name="operadora_num" value=
			<%
				if(aluno != null) {
					for(int i = 0; i < aluno.getTelefones().size(); i++){
						out.print("'"+aluno.getTelefones().get(i).getOperadora().getNumero()+"'/>");	
					}
				} else {
					out.print(">");
				}
			%>
		</input>
		<br>
		<br>
		<label for="logradouro">Endere�o:</label>
		<input type="text" id="logradouro" name="logradouro" value=
			<%
				if(aluno != null) out.print("'"+aluno.getEndereco().getLogradouro()+"'>");
				else out.print(">");
			%>
		</input>
		
		<label for="numero">Num:</label>
		<input type="text" id="numero" name="numero" value=
			<%
				if(aluno != null) out.print("'"+aluno.getEndereco().getNumero()+"'>");
				else out.print(">");
			%>
		</input>
		
		<label for="complemento">Complemento:</label>
		<input type="text" id="complemento" name="complemento" value=
			<%
				if(aluno != null) out.print("'"+aluno.getEndereco().getComplemento()+"'>");
				else out.print(">");
			%>
		</input>
		
		<label for="cep">CEP:</label>
		<input type="text" id="cep" name="cep" value=
			<%
				if(aluno != null) out.print("'"+aluno.getEndereco().getCep()+"'>");
				else out.print(">");
			%>
		</input>
		
		<label for="bairro">Bairro:</label>
		<input type="text" id="bairro" name="bairro" value=
			<%
				if(aluno != null) out.print("'"+aluno.getEndereco().getBairro()+"'>");
				else out.print(">");
			%>
		</input>
		<br>
		<label for="cidade">Cidade:</label>
		<input type="text" id="cidade" name="cidade" value=
			<%
				if(aluno != null) out.print("'"+aluno.getEndereco().getCidade().getNome()+"'>");
				else out.print(">");
			%>
		</input>
		
		<label for="estado">Estado:</label>
		<input type="text" id="estado" name="estado" value=
			<%
				if(aluno != null) out.print("'"+aluno.getEndereco().getCidade().getEstado().getNome()+"'>");
				else out.print(">");
			%>
		</input>
		
		<label for="pais">Pa�s:</label>
		<input type="text" id="pais" name="pais" value=
			<%
				if(aluno != null) out.print("'"+aluno.getEndereco().getCidade().getEstado().getPais().getNome()+"'>");
				else out.print(">");
			%>
		</input>
		
		
		<%
			if(aluno != null){
				String dtCadastro = ConverteDate.converteDateString(aluno.getDtCadastro());
				out.print("<label for='txtDtCadastro'>Data de Cadastro:</label>");
				out.print("<input type='text' id='txtDtCadastro' name='txtDtCadastro' value='"+dtCadastro+"' readonly>");
			}
		%>
		
		<br>
		
		<%	
			if(aluno != null) {
				out.print("<input type='submit' id='operacao' name='operacao' value='ALTERAR'/>");	
				out.print("<input type='submit' id='operacao' name='operacao' value='EXCLUIR'/>");	
			}else{
				out.print("<input type='submit' id='operacao' name='operacao' value='SALVAR'/>");
			}	
		%>
		
	
	</form>
</body>
</html>