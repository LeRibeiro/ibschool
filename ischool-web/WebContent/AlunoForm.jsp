<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>iBS | Cadastrar Aluno</title>
    <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- jQuery 2.2.3 -->
  <script src="AdminLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="AdminLTE/bootstrap/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="AdminLTE/bootstrap/css/bootstrap.min.css" >
  <!-- Font Awesome -->
  <link rel="stylesheet" href="AdminLTE/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="AdminLTE/ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="AdminLTE/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect.
  -->
  <link rel="stylesheet" href="AdminLTE/dist/css/skins/skin-blue.css">
</head>

<body class="hold-transition skin-blue layout-boxed sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="PageInicialAdmin.jsp" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>iB</b>S</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>iB</b>SCHOOL</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <!-- Menu toggle button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">4</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 4 messages</li>
              <li>
                <!-- inner menu: contains the messages -->
                <ul class="menu">
                  <li><!-- start message -->
                    <a href="#">
                      <div class="pull-left">
                      </div>
                      <!-- Message title and timestamp -->
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <!-- The message -->
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <!-- end message -->
                </ul>
                <!-- /.menu -->
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
          <!-- /.messages-menu -->

          <!-- Notifications Menu -->
          <li class="dropdown notifications-menu">
            <!-- Menu toggle button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <!-- Inner Menu: contains the notifications -->
                <ul class="menu">
                  <li><!-- start notification -->
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                  <!-- end notification -->
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- Tasks Menu -->
          <li class="dropdown tasks-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">9</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 9 tasks</li>
              <li>
                <!-- Inner menu: contains the tasks -->
                <ul class="menu">
                  <li><!-- Task item -->
                    <a href="#">
                      <!-- Task title and progress text -->
                      <h3>
                        Design some buttons
                        <small class="pull-right">20%</small>
                      </h3>
                      <!-- The progress bar -->
                      <div class="progress xs">
                        <!-- Change the css width attribute to simulate progress -->
                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img src="#" class="user-image" alt="User Image">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs">Leticia Ribeiro</span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <p>
                  Leticia Ribeiro
                  <small>Administrador</small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="Login.jsp" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar and menu -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="header">OP��ES</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="treeview">
          <a href="#">
          	<i class="fa fa-mortar-board"></i> <span>Alunos</span>
          	<span class="pull-right-container">
    	    	<i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
            	<a href="AlunoForm.jsp">Cadastrar</a>
            </li>
            <li>
            	<a href="ConsultaAlunoForm.jsp">Consultar</a>
           	</li>
          </ul>
        </li>
        <li class="treeview">
			<a href="#"> 
					<i class="fa fa-users"></i> <span>Turmas</span> 
				<span class="pull-right-container"> 
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="GerenciarTurmas.jsp">Gerenciar</a></li>
				<li><a href="VincularAluno.jsp">Vincular aluno</a></li>
			</ul>
		</li>
        <li>
        	<a href="ProfessorForm.jsp">
        		<i class="fa fa-user"></i> <span>Professores</span>
        	</a>
        </li>
        <li>
        	<a href="Calendario.jsp">
        		<i class="fa fa-calendar"></i> <span>Calend�rio</span>
        	</a>
        </li>
        <li>
        	<a href="#">
        		<i class="fa fa-bar-chart"></i> <span>An�lises</span>
        	</a>
        </li>
        <li class="treeview">
			<a href="#"> <i class="fa fa-dollar"></i> <span>Financeiro</span> 
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="AdicionarCreditos.jsp">Adicionar cr�ditos</a></li>					
			</ul>
		</li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <i class="fa fa-plus"></i> <span>Cadastro Aluno</span>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

	<!-- Main content -->
	<!-- form start -->
	<div class="box-body">
		<div class="col-xs-12 ">
			<div class="box">
				<div class="box-body nav-tabs-custom">
					<ul class="nav nav-tabs"></ul>
					<div class="box-header with-border">
						<i class="fa fa-user-plus"></i>
						<h3 class="box-title">Dados Pessoais</h3>
						
					</div>

					<!-- CAMPO DE DADOS PESSOAIS -->
					<div class="row">
						<div class="form-group col-md-9">
							<label for="txtNome">Nome</label> <input type="text"
								class="form-control" id="txtNome" name="txtNome"
								placeholder="Nome">
						</div>
						<div class="form-group col-md-3">
							<label for="txtDtNascimento">Data de Nascimento</label> <input
								type="date" class="form-control" id="txtDtNascimento"
								name="txtDtNascimento">
						</div>
					</div>
					<div>
						<div class="row">
							<div class="form-group col-md-4">
								<label for="txtCpf">CPF</label> <input type="text"
									class="form-control" id="txtCpf" name="txtCpf"
									placeholder="CPF">
							</div>
							<div class="form-group col-md-4">
								<label for="txtRg">RG</label> <input type="text"
									class="form-control" id="txtRg" name="txtRg"
									placeholder="RG">
							</div>
							<div class="form-group col-md-4">
								<label for="txtEmail">E-mail</label> <input type="text"
									class="form-control" id="txtEmail" name="txtEmail"
									placeholder="E-mail">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-2">
								<div class="radio">
									<label> <input id="radio_genero" name="radio_genero"
										value="Masculino" type="radio"></input><b>Masculino</b>
									</label>
								</div>
							</div>
							<div class="form-group col-md-2">
								<div class="radio">
									<label> <input id="radio_genero" name="radio_genero"
										value="Feminino" type="radio"></input><b>Feminino</b>
									</label>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-2">
									<label for="txtTelefone">Tel. Residencial</label> <input
										type="text" class="form-control" id="txtTelefone"
										name="txtTelefone" placeholder="Telefone">
								</div>
								<div class="form-group col-md-3">
									<label for="txtCelular">Celular</label> <input type="text"
										class="form-control" id="txtCelular" name="txtCelular"
										placeholder="Celular">
								</div>
								<div class="form-group col-md-2">
									<label for="txtOperadora">Operadora</label> 
									<select class="form-control" id="txtOperadora" name="txtOperadora">
										<option value="">Selecione</option>
										<option value="VIVO">VIVO</option>
										<option value="TIM">TIM</option>
										<option value="OI">OI</option>
										<option value="CLARO">CLARO</option>
									</select>
								</div>
							</div>
						</div>
					</div>

					<!-- CAMPO DE ENDERE�O -->
					<div class="box-header with-border">
						<i class="fa fa-user-plus"></i>
						<h3 class="box-title">Endere�o</h3>
					</div>
					<div class="row">
						<div class="form-group col-md-6">
							<label for="txtEndereco">Rua</label> <input type="text"
								class="form-control" id="txtEndereco" name="txtEndereco"
								placeholder="Endere�o">
						</div>
						<div class="form-group col-md-4">
							<label for="txtCep">CEP</label> <input type="text"
								class="form-control" id="txtCep" name="txtCep">
						</div>
						<div class="form-group col-md-2">
							<label for="txtDtNascimento">N�mero</label> <input type="text"
								class="form-control" id="txtNumero" name="txtNumero">
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-3">
							<label for="txtComplemento">Complemento</label> <input
								type="text" class="form-control" id="txtComplemento"
								name="txtComplemento" placeholder="Complemento">
						</div>
						<div class="form-group col-md-4">
							<label for="txtBairro">Bairro</label> <input type="text"
								class="form-control" id="txtBairro" name="txtBairro"
								placeholder="Bairro">
						</div>
						<div class="form-group col-md-3">
							<label for="txtCidade">Cidade</label> <input type="text"
								class="form-control" id="txtCidade" name="txtCidade"
								placeholder="Cidade">
						</div>
						<div class="form-group col-md-2">
							<label for="txtEstado">Estado</label> <select
								class="form-control" id="txtEstado" name="txtEstado">
								<option value="">Selecione</option>
								<option value="AC">Acre</option>
								<option value="AL">Alagoas</option>
								<option value="AP">Amap�</option>
								<option value="AM">Amazonas</option>
								<option value="BA">Bahia</option>
								<option value="CE">Cear�</option>
								<option value="DF">Distrito Federal</option>
								<option value="ES">Espirito Santo</option>
								<option value="GO">Goi�s</option>
								<option value="MA">Maranh�o</option>
								<option value="MS">Mato Grosso do Sul</option>
								<option value="MT">Mato Grosso</option>
								<option value="MG">Minas Gerais</option>
								<option value="PA">Par�</option>
								<option value="PB">Para�ba</option>
								<option value="PR">Paran�</option>
								<option value="PE">Pernambuco</option>
								<option value="PI">Piau�</option>
								<option value="RJ">Rio de Janeiro</option>
								<option value="RN">Rio Grande do Norte</option>
								<option value="RS">Rio Grande do Sul</option>
								<option value="RO">Rond�nia</option>
								<option value="RR">Roraima</option>
								<option value="SC">Santa Catarina</option>
								<option value="SP">S�o Paulo</option>
								<option value="SE">Sergipe</option>
								<option value="TO">Tocantins</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-6">
							<label for="txtPais">Pa�s</label> <input type="text"
								class="form-control" id="txtPais" name="txtPais"
								placeholder="Pa�s">
						</div>
						<br>

					</div>
					<div class="row">
						<div class="form-group col-md-11">
							<button type="submit" class="btn btn-warning pull-right"
								name="operacao" id="operacao" value="SALVAR" onclick="salvarAluno()">Cadastrar</button>
						</div>
					</div>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.content-wrapper -->




   <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript::;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript::;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                  <span class="label label-danger pull-right">70%</span>
                </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
  

  
</div>
 <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
  </footer>
<!-- ./wrapper -->

<!-- AdminLTE App -->
<!-- � necess�rio deixar no final da p�gina! -->
<script src="AdminLTE/dist/js/app.min.js"></script>
<script src="AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js"></script>
</body>
</html>