<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>iBS | Gerenciar Turmas</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="AdminLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="AdminLTE/bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="AdminLTE/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="AdminLTE/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="AdminLTE/ionicons/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="AdminLTE/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect.
  -->
<link rel="stylesheet" href="AdminLTE/dist/css/skins/skin-blue.css">
</head>

<body class="hold-transition skin-blue layout-boxed sidebar-mini">
	<div class="wrapper">

		<!-- Main Header -->
		<header class="main-header"> <!-- Logo --> <a
			href="PageInicialAdmin.jsp" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
			<span class="logo-mini"><b>iB</b>S</span> <!-- logo for regular state and mobile devices -->
			<span class="logo-lg"><b>iB</b>SCHOOL</span>
		</a> <!-- Header Navbar --> <nav class="navbar navbar-static-top"
			role="navigation"> <!-- Sidebar toggle button--> <a href="#"
			class="sidebar-toggle" data-toggle="offcanvas" role="button"> <span
			class="sr-only">Toggle navigation</span>
		</a> <!-- Navbar Right Menu -->
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<!-- Messages: style can be found in dropdown.less-->
				<li class="dropdown messages-menu">
					<!-- Menu toggle button --> <a href="#" class="dropdown-toggle"
					data-toggle="dropdown"> <i class="fa fa-envelope-o"></i> <span
						class="label label-success">4</span>
				</a>
					<ul class="dropdown-menu">
						<li class="header">You have 4 messages</li>
						<li>
							<!-- inner menu: contains the messages -->
							<ul class="menu">
								<li>
									<!-- start message --> <a href="#">
										<div class="pull-left"></div> <!-- Message title and timestamp -->
										<h4>
											Support Team <small><i class="fa fa-clock-o"></i> 5
												mins</small>
										</h4> <!-- The message -->
										<p>Why not buy a new awesome theme?</p>
								</a>
								</li>
								<!-- end message -->
							</ul> <!-- /.menu -->
						</li>
						<li class="footer"><a href="#">See All Messages</a></li>
					</ul>
				</li>
				<!-- /.messages-menu -->

				<!-- Notifications Menu -->
				<li class="dropdown notifications-menu">
					<!-- Menu toggle button --> <a href="#" class="dropdown-toggle"
					data-toggle="dropdown"> <i class="fa fa-bell-o"></i> <span
						class="label label-warning">10</span>
				</a>
					<ul class="dropdown-menu">
						<li class="header">You have 10 notifications</li>
						<li>
							<!-- Inner Menu: contains the notifications -->
							<ul class="menu">
								<li>
									<!-- start notification --> <a href="#"> <i
										class="fa fa-users text-aqua"></i> 5 new members joined today
								</a>
								</li>
								<!-- end notification -->
							</ul>
						</li>
						<li class="footer"><a href="#">View all</a></li>
					</ul>
				</li>
				<!-- Tasks Menu -->
				<li class="dropdown tasks-menu">
					<!-- Menu Toggle Button --> <a href="#" class="dropdown-toggle"
					data-toggle="dropdown"> <i class="fa fa-flag-o"></i> <span
						class="label label-danger">9</span>
				</a>
					<ul class="dropdown-menu">
						<li class="header">You have 9 tasks</li>
						<li>
							<!-- Inner menu: contains the tasks -->
							<ul class="menu">
								<li>
									<!-- Task item --> <a href="#"> <!-- Task title and progress text -->
										<h3>
											Design some buttons <small class="pull-right">20%</small>
										</h3> <!-- The progress bar -->
										<div class="progress xs">
											<!-- Change the css width attribute to simulate progress -->
											<div class="progress-bar progress-bar-aqua"
												style="width: 20%" role="progressbar" aria-valuenow="20"
												aria-valuemin="0" aria-valuemax="100">
												<span class="sr-only">20% Complete</span>
											</div>
										</div>
								</a>
								</li>
								<!-- end task item -->
							</ul>
						</li>
						<li class="footer"><a href="#">View all tasks</a></li>
					</ul>
				</li>
				<!-- User Account Menu -->
				<li class="dropdown user user-menu">
					<!-- Menu Toggle Button --> <a href="#" class="dropdown-toggle"
					data-toggle="dropdown"> <!-- The user image in the navbar--> <img
						src="#" class="user-image"
						alt="User Image"> <!-- hidden-xs hides the username on small devices so only the image appears. -->
						<span class="hidden-xs">Leticia Ribeiro</span>
				</a>
					<ul class="dropdown-menu">
						<!-- The user image in the menu -->
						<li class="user-header">
							<p>
								Leticia Ribeiro <small>Administrador</small>
							</p>
						</li>
						<!-- Menu Body -->
						<li class="user-body">
							<div class="row">
								<div class="col-xs-4 text-center">
									<a href="#">Followers</a>
								</div>
								<div class="col-xs-4 text-center">
									<a href="#">Sales</a>
								</div>
								<div class="col-xs-4 text-center">
									<a href="#">Friends</a>
								</div>
							</div> <!-- /.row -->
						</li>
						<!-- Menu Footer-->
						<li class="user-footer">
							<div class="pull-left">
								<a href="#" class="btn btn-default btn-flat">Profile</a>
							</div>
							<div class="pull-right">
								<a href="Login.jsp" class="btn btn-default btn-flat">Sign
									out</a>
							</div>
						</li>
					</ul>
				</li>
				<!-- Control Sidebar Toggle Button -->
				<li><a href="#" data-toggle="control-sidebar"><i
						class="fa fa-gears"></i></a></li>
			</ul>
		</div>
		</nav> </header>
		<!-- Left side column. contains the logo and sidebar and menu -->
		<aside class="main-sidebar"> <!-- sidebar: style can be found in sidebar.less -->
		<section class="sidebar"> <!-- Sidebar Menu -->
		<ul class="sidebar-menu">
			<li class="header">OP��ES</li>
			<!-- Optionally, you can add icons to the links -->
			<li class="treeview">
				<a href="#"> 
						<i class="fa fa-mortar-board"></i> <span>Alunos</span> 
					<span class="pull-right-container"> 
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="AlunoForm.jsp">Cadastrar</a></li>
					<li><a href="ConsultaAlunoForm.jsp">Consultar</a></li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#"> 
						<i class="fa fa-users"></i> <span>Turmas</span> 
					<span class="pull-right-container"> 
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="GerenciarTurmas.jsp">Gerenciar</a></li>
					<li><a href="VincularAluno.jsp">Vincular aluno</a></li>
				</ul>
			</li>
			<li>
				<a href="ProfessorForm.jsp"> 
					<i class="fa fa-user"></i><span>Professores</span>
				</a>
			</li>
			<li>
				<a href="Calendario.jsp"> 
					<i class="fa fa-calendar"></i><span>Calend�rio</span>
				</a>
			</li>
			<li>
				<a href="#"> 
					<i class="fa fa-bar-chart"></i> <span>An�lises</span>
				</a>
			</li>
			<li class="treeview">
				<a href="#"> <i class="fa fa-dollar"></i> <span>Financeiro</span> 
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="AdicionarCreditos.jsp">Adicionar cr�ditos</a></li>					
				</ul>
			</li>
		</ul>
		<!-- /.sidebar-menu --> </section> <!-- /.sidebar --> </aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<div class="box-body">
				<div class="col-md-12">
					<div class="box box-danger">
						<div class="box-body">
							<div class="box-header with-border" style="text-align: center;">
								<i class="fa fa-users"></i>
								<h3 class="box-title">Cadastrar nova turma</h3>
								<button type="button" class="btn btn-default disabled pull-right"
									name="cancelar" id="cancelar">Cancelar</button>
							</div>
							<div class="row">
								<div class="form-group col-md-6">
									<label>Nome da turma</label> <input type="text"
										class="form-control" name="txtNome" id="txtNome"
										placeholder="Nome da turma">
								</div>
								<div class="form-group col-md-3">
									<label for="categoria">Professor:</label> <select
										class="form-control" id="categoria">
										<option value="">Bene</option>
										<option value="">Luciana</option>
										<option value="">Leticia</option>
									</select>
								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-3">
									<label for="txtQuantidade">Aluno 1:</label> 
									<select
										class="form-control" id="categoria">
										<option value="">Gustavo</option>
										<option value="">Bruno</option>
										<option value="">Leticia</option>
									</select>
								</div>
								<div class="form-group col-md-3">
									<label for="txtQuantidade">Aluno 2:</label> 
									<select
										class="form-control" id="categoria">
										<option value="">Gustavo</option>
										<option value="">Bruno</option>
										<option value="">Leticia</option>
									</select>
								</div>
								<div class="form-group col-md-3">
								<br>
								<label>								
									<a>Adicionar mais alunos</a>
								</label>									
								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-3">
									<label for="dia">Dia da semana:</label> 
									<select class="form-control"
										id="dia">
										<option value="">Segunda</option>
										<option value="">Ter�a</option>
										<option value="">Quarta</option>
										<option value="">Quinta</option>
										<option value="">Sexta</option>
										<option value="">Sabado</option>
									</select>
								</div>
								<div class="form-group col-md-2">
									<label for="inicio">Hora inicio:</label>
									<br> 
									<input type="time" id="inicio"/>
								</div>
								<div class="form-group col-md-3">
									<label for="fim">Hora fim:</label>
									<br> 
									<input type="time" id="fim"/>
								</div>
							</div>

							<!-- SEGUNDA LINHA -->
							<div class="row">
								<div class="form-group col-md-11">
									<button type="submit" class="btn btn-primary pull-right"
										name="operacao" id="salvar" value="SALVAR">Cadastrar</button>
									<button type="submit" class="btn btn-warning pull-right"
										id="alterar" name="operacao" value="ALTERAR">Alterar</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- CONSULTAR -->
			<div class="box-body">
				<div class="col-md-12">
					<div class="box box-danger">
						<div class="box-body">
							<div class="box-header with-border" style="text-align: center;">
								<i class="fa fa-list"></i>
								<h3 class="box-title">Consultar turmas</h3>
							</div>
							<table class="table table-striped">
								<tr>
									<th>ID da turma</th>
									<th>Nome</th>
									<th>Professor</th>
									<th>Alunos</th>
									<th>Data/Hora</th>
									<th>Op��es</th>
								</tr>
								<tr>
									<td><a> 1 </a></td>
									<td>Livro 1A</td>
									<td>Bene</td>
									<td>Gustavo, Bruno</td>
									<td>Sabado - 15hrs at� 16:30hrs</td>
									<td>
										<button type='button' class='btn btn-warning'>Alterar</button>
										<button type='button' class='btn btn-danger'>Excluir</button>
										<a href="VincularAluno.jsp">
											<button type='button' class='btn btn-primary'>Vincular alunos</button>
										</a>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.content-wrapper -->

		<!-- Main Footer -->
		<footer class="main-footer"> <!-- To the right -->
		<div class="pull-right hidden-xs">Anything you want</div>
		<!-- Default to the left --> <strong>Copyright &copy; 2016 <a
			href="#">Company</a>.
		</strong> All rights reserved. </footer>

		<!-- Control Sidebar -->
		<aside class="control-sidebar control-sidebar-dark"> <!-- Create the tabs -->
		<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
			<li class="active"><a href="#control-sidebar-home-tab"
				data-toggle="tab"><i class="fa fa-home"></i></a></li>
			<li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i
					class="fa fa-gears"></i></a></li>
		</ul>
		<!-- Tab panes -->
		<div class="tab-content">
			<!-- Home tab content -->
			<div class="tab-pane active" id="control-sidebar-home-tab">
				<h3 class="control-sidebar-heading">Recent Activity</h3>
				<ul class="control-sidebar-menu">
					<li><a href="javascript::;"> <i
							class="menu-icon fa fa-birthday-cake bg-red"></i>

							<div class="menu-info">
								<h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

								<p>Will be 23 on April 24th</p>
							</div>
					</a></li>
				</ul>
				<!-- /.control-sidebar-menu -->

				<h3 class="control-sidebar-heading">Tasks Progress</h3>
				<ul class="control-sidebar-menu">
					<li><a href="javascript::;">
							<h4 class="control-sidebar-subheading">
								Custom Template Design <span class="pull-right-container">
									<span class="label label-danger pull-right">70%</span>
								</span>
							</h4>

							<div class="progress progress-xxs">
								<div class="progress-bar progress-bar-danger" style="width: 70%"></div>
							</div>
					</a></li>
				</ul>
				<!-- /.control-sidebar-menu -->

			</div>
			<!-- /.tab-pane -->
			<!-- Stats tab content -->
			<div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab
				Content</div>
			<!-- /.tab-pane -->
			<!-- Settings tab content -->
			<div class="tab-pane" id="control-sidebar-settings-tab">
				<form method="post">
					<h3 class="control-sidebar-heading">General Settings</h3>

					<div class="form-group">
						<label class="control-sidebar-subheading"> Report panel
							usage <input type="checkbox" class="pull-right" checked>
						</label>

						<p>Some information about this general settings option</p>
					</div>
					<!-- /.form-group -->
				</form>
			</div>
			<!-- /.tab-pane -->
		</div>
		</aside>
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

	<!-- AdminLTE App -->
	<!-- � necess�rio deixar no final da p�gina! -->
	<script src="AdminLTE/dist/js/app.min.js"></script>
	<script src="AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js"></script>
</body>
</html>