<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>iBS | Calend�rio</title>
    <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- jQuery 2.2.3 -->
  <script src="AdminLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="AdminLTE/bootstrap/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="AdminLTE/bootstrap/css/bootstrap.min.css" >
  <!-- Font Awesome -->
  <link rel="stylesheet" href="AdminLTE/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="AdminLTE/ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="AdminLTE/dist/css/AdminLTE.min.css">
  <!-- Full Calendar -->
  <link rel="stylesheet" href="AdminLTE/plugins/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" href="AdminLTE/plugins/fullcalendar/fullcalendar.print.css">
  <link rel="stylesheet" href="AdminLTE/plugins/fullcalendar/fullcalendar.min.js">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect.
  -->
  <link rel="stylesheet" href="AdminLTE/dist/css/skins/skin-blue.css">
</head>

<body class="hold-transition skin-blue layout-boxed sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="PageInicialAdmin.jsp" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>iB</b>S</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>iB</b>SCHOOL</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <!-- Menu toggle button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">4</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 4 messages</li>
              <li>
                <!-- inner menu: contains the messages -->
                <ul class="menu">
                  <li><!-- start message -->
                    <a href="#">
                      <div class="pull-left">
                      </div>
                      <!-- Message title and timestamp -->
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <!-- The message -->
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <!-- end message -->
                </ul>
                <!-- /.menu -->
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
          <!-- /.messages-menu -->

          <!-- Notifications Menu -->
          <li class="dropdown notifications-menu">
            <!-- Menu toggle button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <!-- Inner Menu: contains the notifications -->
                <ul class="menu">
                  <li><!-- start notification -->
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                  <!-- end notification -->
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- Tasks Menu -->
          <li class="dropdown tasks-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">9</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 9 tasks</li>
              <li>
                <!-- Inner menu: contains the tasks -->
                <ul class="menu">
                  <li><!-- Task item -->
                    <a href="#">
                      <!-- Task title and progress text -->
                      <h3>
                        Design some buttons
                        <small class="pull-right">20%</small>
                      </h3>
                      <!-- The progress bar -->
                      <div class="progress xs">
                        <!-- Change the css width attribute to simulate progress -->
                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img src="#" class="user-image" alt="User Image">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs">Leticia Ribeiro</span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <p>
                  Leticia Ribeiro
                  <small>Administrador</small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="Login.jsp" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar and menu -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="header">OP��ES</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="treeview">
          <a href="#">
          	<i class="fa fa-mortar-board"></i> <span>Alunos</span>
          	<span class="pull-right-container">
    	    	<i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
            	<a href="AlunoForm.jsp">Cadastrar</a>
            </li>
            <li>
            	<a href="#">Consultar</a>
           	</li>
          </ul>
        </li>
        <li class="treeview">
			<a href="#"> 
					<i class="fa fa-users"></i> <span>Turmas</span> 
				<span class="pull-right-container"> 
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="GerenciarTurmas.jsp">Gerenciar</a></li>
				<li><a href="VincularAluno.jsp">Vincular aluno</a></li>
			</ul>
		</li>
        <li>
        	<a href="ProfessorForm.jsp"> 
        		<i class="fa fa-user"></i> <span>Professores</span>
        	</a>
        </li>
        <li>
        	<a href="Calendario.jsp">
        		<i class="fa fa-calendar"></i> <span>Calend�rio</span>
        	</a>
        </li>
        <li>
        	<a href="#">
        		<i class="fa fa-bar-chart"></i> <span>An�lises</span>
        	</a>
        </li>
        <li class="treeview">
			<a href="#"> <i class="fa fa-dollar"></i> <span>Financeiro</span> 
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="AdicionarCreditos.jsp">Adicionar cr�ditos</a></li>				
			</ul>
		</li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content">
    	<!-- THE CALENDAR -->
    	<div class="col-md-10 col-md-push-1">
			<div class="box box-danger">
		      	<div class="box-body no-padding">
		        	<div id="calendar" class="fc fc-ltr fc-unthemed">
		        		<div class="fc-toolbar">
		            		<div class="fc-left">
		            			<div class="fc-button-group">
		            				<button type="button" class="fc-prev-button fc-button fc-state-default fc-corner-left">
		            					<span class="fc-icon fc-icon-left-single-arrow"></span>
		            				</button>
		            				<button type="button" class="fc-next-button fc-button fc-state-default fc-corner-right">
		            					<span class="fc-icon fc-icon-right-single-arrow"></span>
		            				</button>
		            			</div>
		            			<button type="button" class="fc-today-button fc-button fc-state-default fc-corner-left fc-corner-right fc-state-disabled" disabled="disabled">today</button>
		            		</div>
		            		<div class="fc-right">
		            			<div class="fc-button-group">
		            				<button type="button" class="fc-month-button fc-button fc-state-default fc-corner-left fc-state-active">month</button>
		            				<button type="button" class="fc-agendaWeek-button fc-button fc-state-default">week</button>
		            				<button type="button" class="fc-agendaDay-button fc-button fc-state-default fc-corner-right">day</button>
		            			</div>
		            		</div>
		            		<div class="fc-center">
		            			<h2>September 2016</h2>
		            		</div>
		            		<div class="fc-clear">
		            		</div>
		            		<div class="fc-view-container" style="">
	            		<div class="fc-view fc-month-view fc-basic-view">
	            			<table>
	            				<thead>
	            					<tr>
	            						<td class="fc-widget-header">
	            							<div class="fc-row fc-widget-header">
	            								<table>
	            									<thead>
	            										<tr>
	            											<th class="fc-day-header fc-widget-header fc-sun">Sun</th>
	            											<th class="fc-day-header fc-widget-header fc-mon">Mon</th>
	            											<th class="fc-day-header fc-widget-header fc-tue">Tue</th>
	            											<th class="fc-day-header fc-widget-header fc-wed">Wed</th>
	            											<th class="fc-day-header fc-widget-header fc-thu">Thu</th>
	            											<th class="fc-day-header fc-widget-header fc-fri">Fri</th>
	            											<th class="fc-day-header fc-widget-header fc-sat">Sat</th>
	            										</tr>
	            									</thead>
	            								</table>
	            							</div>
	            						</td>
	            					</tr>
	            				</thead>
	            				<tbody>
	            					<tr>
	            						<td class="fc-widget-content">
	            							<div class="fc-day-grid-container">
	            								<div class="fc-day-grid">
	            									<div class="fc-row fc-week fc-widget-content" style="height: 72px;">
	            										<div class="fc-bg">
	            											<table>
	            												<tbody>
	            													<tr>
	            														<td class="fc-day fc-widget-content fc-sun fc-other-month fc-past" data-date="2016-08-28"></td>
	            														<td class="fc-day fc-widget-content fc-mon fc-other-month fc-past" data-date="2016-08-29"></td>
	            														<td class="fc-day fc-widget-content fc-tue fc-other-month fc-past" data-date="2016-08-30"></td>
	            														<td class="fc-day fc-widget-content fc-wed fc-other-month fc-past" data-date="2016-08-31"></td>
	            														<td class="fc-day fc-widget-content fc-thu fc-past" data-date="2016-09-01"></td>
	            														<td class="fc-day fc-widget-content fc-fri fc-past" data-date="2016-09-02"></td>
	            														<td class="fc-day fc-widget-content fc-sat fc-past" data-date="2016-09-03"></td>
	            													</tr>
	            												</tbody>
	            											</table>
	            										</div>
	            										<div class="fc-content-skeleton">
	            											<table>
	            												<thead>
	            													<tr>
	            														<td class="fc-day-number fc-sun fc-other-month fc-past" data-date="2016-08-28">28</td>
	            														<td class="fc-day-number fc-mon fc-other-month fc-past" data-date="2016-08-29">29</td>
	            														<td class="fc-day-number fc-tue fc-other-month fc-past" data-date="2016-08-30">30</td>
	            														<td class="fc-day-number fc-wed fc-other-month fc-past" data-date="2016-08-31">31</td>
	            														<td class="fc-day-number fc-thu fc-past" data-date="2016-09-01">1</td>
	            														<td class="fc-day-number fc-fri fc-past" data-date="2016-09-02">2</td>
	            														<td class="fc-day-number fc-sat fc-past" data-date="2016-09-03">3</td>
	            													</tr>
	            												</thead>
	            												<tbody>
	            													<tr>
	            														<td></td>
	            														<td></td>
	            														<td></td>
	            														<td></td>
	            														<td class="fc-event-container">
	            															<a class="fc-day-grid-event fc-event fc-start fc-end fc-draggable" 
	            															style="background-color:#f56954;border-color:#f56954">
	            																<div class="fc-content"><span class="fc-time">12a</span> 
	            																	<span class="fc-title">All Day Event</span>
	            																</div>
	            															</a>
	            														</td>
	            														<td></td>
	            														<td></td>
	            													</tr>
	            												</tbody>
	            											</table>
	            										</div>
	            									</div>
	            									<div class="fc-row fc-week fc-widget-content" style="height: 72px;">
		            									<div class="fc-bg">
		            										<table>
		            											<tbody>
			            											<tr>
			            												<td class="fc-day fc-widget-content fc-sun fc-past" data-date="2016-09-04"></td>
			            												<td class="fc-day fc-widget-content fc-mon fc-past" data-date="2016-09-05"></td>
			            												<td class="fc-day fc-widget-content fc-tue fc-past" data-date="2016-09-06"></td>
			            												<td class="fc-day fc-widget-content fc-wed fc-past" data-date="2016-09-07"></td>
			            												<td class="fc-day fc-widget-content fc-thu fc-past" data-date="2016-09-08"></td>
			            												<td class="fc-day fc-widget-content fc-fri fc-past" data-date="2016-09-09"></td>
			            												<td class="fc-day fc-widget-content fc-sat fc-past" data-date="2016-09-10"></td>
			            											</tr>
		            											</tbody>
		            										</table>
		            									</div>
		            									<div class="fc-content-skeleton">
		            										<table>
		            											<thead>
		            												<tr>
		            													<td class="fc-day-number fc-sun fc-past" data-date="2016-09-04">4</td>
		            													<td class="fc-day-number fc-mon fc-past" data-date="2016-09-05">5</td>
		            													<td class="fc-day-number fc-tue fc-past" data-date="2016-09-06">6</td>
		            													<td class="fc-day-number fc-wed fc-past" data-date="2016-09-07">7</td>
		            													<td class="fc-day-number fc-thu fc-past" data-date="2016-09-08">8</td>
		            													<td class="fc-day-number fc-fri fc-past" data-date="2016-09-09">9</td>
		            													<td class="fc-day-number fc-sat fc-past" data-date="2016-09-10">10</td>
		            												</tr>
		            											</thead>
		            											<tbody>
		            												<tr>
		            													<td></td>
		            													<td></td>
		            													<td></td>
		            													<td></td>
		            													<td></td>
		            													<td></td>
		            													<td></td>
		            												</tr>
		            											</tbody>
		            										</table>
		            									</div>
	            									</div>
	            									<div class="fc-row fc-week fc-widget-content" style="height: 72px;">
	            										<div class="fc-bg">
	            											<table>
	            												<tbody>
	            													<tr>
	            														<td class="fc-day fc-widget-content fc-sun fc-past" data-date="2016-09-11"></td>
	            														<td class="fc-day fc-widget-content fc-mon fc-past" data-date="2016-09-12"></td>
	            														<td class="fc-day fc-widget-content fc-tue fc-past" data-date="2016-09-13"></td>
	            														<td class="fc-day fc-widget-content fc-wed fc-past" data-date="2016-09-14"></td>
	            														<td class="fc-day fc-widget-content fc-thu fc-past" data-date="2016-09-15"></td>
	            														<td class="fc-day fc-widget-content fc-fri fc-past" data-date="2016-09-16"></td>
	            														<td class="fc-day fc-widget-content fc-sat fc-past" data-date="2016-09-17"></td>
	            													</tr>
	            												</tbody>
	            											</table>
	            										</div>
	            										<div class="fc-content-skeleton">
	            											<table>
	            												<thead>
	            													<tr>
	            														<td class="fc-day-number fc-sun fc-past" data-date="2016-09-11">11</td>
	            														<td class="fc-day-number fc-mon fc-past" data-date="2016-09-12">12</td>
	            														<td class="fc-day-number fc-tue fc-past" data-date="2016-09-13">13</td>
	            														<td class="fc-day-number fc-wed fc-past" data-date="2016-09-14">14</td>
	            														<td class="fc-day-number fc-thu fc-past" data-date="2016-09-15">15</td>
	            														<td class="fc-day-number fc-fri fc-past" data-date="2016-09-16">16</td>
	            														<td class="fc-day-number fc-sat fc-past" data-date="2016-09-17">17</td>
	            													</tr>
	            												</thead>
	            												<tbody>
	            													<tr>
	            														<td></td>
	            														<td></td>
	            														<td></td>
	            														<td></td>
	            														<td></td>
	            														<td></td>
	            														<td></td>
	            													</tr>
	            												</tbody>
	            											</table>
	            										</div>
	            									</div>
	            									<div class="fc-row fc-week fc-widget-content" style="height: 72px;">
	            										<div class="fc-bg">
	            											<table>
	            												<tbody>
	            													<tr>
	            														<td class="fc-day fc-widget-content fc-sun fc-past" data-date="2016-09-18"></td>
	            														<td class="fc-day fc-widget-content fc-mon fc-past" data-date="2016-09-19"></td>
	            														<td class="fc-day fc-widget-content fc-tue fc-past" data-date="2016-09-20"></td>
	            														<td class="fc-day fc-widget-content fc-wed fc-past" data-date="2016-09-21"></td>
	            														<td class="fc-day fc-widget-content fc-thu fc-past" data-date="2016-09-22"></td>
	            														<td class="fc-day fc-widget-content fc-fri fc-past" data-date="2016-09-23"></td>
	            														<td class="fc-day fc-widget-content fc-sat fc-past" data-date="2016-09-24"></td>
	            													</tr>
	            												</tbody>
	            											</table>
	            										</div>
	            										<div class="fc-content-skeleton">
	            											<table>
	            												<thead>
	            													<tr>
	            														<td class="fc-day-number fc-sun fc-past" data-date="2016-09-18">18</td>
	            														<td class="fc-day-number fc-mon fc-past" data-date="2016-09-19">19</td>
	            														<td class="fc-day-number fc-tue fc-past" data-date="2016-09-20">20</td>
	            														<td class="fc-day-number fc-wed fc-past" data-date="2016-09-21">21</td>
	            														<td class="fc-day-number fc-thu fc-past" data-date="2016-09-22">22</td>
	            														<td class="fc-day-number fc-fri fc-past" data-date="2016-09-23">23</td>
	            														<td class="fc-day-number fc-sat fc-past" data-date="2016-09-24">24</td>
	            													</tr>
	            												</thead>
	            												<tbody>
	            													<tr>
	            														<td></td>
	            														<td></td>
	            														<td></td>
	            														<td></td>
	            														<td class="fc-event-container" colspan="3">
	            															<a class="fc-day-grid-event fc-event fc-start fc-end fc-draggable" 
	            															style="background-color:#f39c12;border-color:#f39c12">
	            																<div class="fc-content"><span class="fc-time">12a</span> 
	            																	<span class="fc-title">Long Event</span>
	            																</div>
	            															</a>
	            														</td>
	            													</tr>
	            												</tbody>
	            											</table>
	            										</div>
	            									</div>
	            									<div class="fc-row fc-week fc-widget-content" style="height: 72px;">
	            										<div class="fc-bg">
	            											<table>
	            												<tbody>
	            													<tr>
	            														<td class="fc-day fc-widget-content fc-sun fc-past" data-date="2016-09-25"></td>
	            														<td class="fc-day fc-widget-content fc-mon fc-past" data-date="2016-09-26"></td>
	            														<td class="fc-day fc-widget-content fc-tue fc-today fc-state-highlight" data-date="2016-09-27"></td>
	            														<td class="fc-day fc-widget-content fc-wed fc-future" data-date="2016-09-28"></td>
	            														<td class="fc-day fc-widget-content fc-thu fc-future" data-date="2016-09-29"></td>
	            														<td class="fc-day fc-widget-content fc-fri fc-future" data-date="2016-09-30"></td>
	            														<td class="fc-day fc-widget-content fc-sat fc-other-month fc-future" data-date="2016-10-01"></td>
	            													</tr>
	            												</tbody>
	            											</table>
	            										</div>
	            									<div class="fc-content-skeleton">
	            									<table>
	            										<thead>
	            											<tr>
	            												<td class="fc-day-number fc-sun fc-past" data-date="2016-09-25">25</td>
	            												<td class="fc-day-number fc-mon fc-past" data-date="2016-09-26">26</td>
	            												<td class="fc-day-number fc-tue fc-today fc-state-highlight" data-date="2016-09-27">27</td>
	            												<td class="fc-day-number fc-wed fc-future" data-date="2016-09-28">28</td>
	            												<td class="fc-day-number fc-thu fc-future" data-date="2016-09-29">29</td>
	            												<td class="fc-day-number fc-fri fc-future" data-date="2016-09-30">30</td>
	            												<td class="fc-day-number fc-sat fc-other-month fc-future" data-date="2016-10-01">1</td>
	            											</tr>
	            										</thead>
	            										<tbody>
	            											<tr>
	            												<td rowspan="2"></td>
	            												<td rowspan="2"></td>
	            												<td class="fc-event-container">
	            													<a class="fc-day-grid-event fc-event fc-start fc-end fc-draggable" style="background-color:#0073b7;border-color:#0073b7">
	            														<div class="fc-content">
	            															<span class="fc-time">10:30a</span> 
	            															<span class="fc-title">Meeting</span>
	            														</div>
	            													</a>
	            												</td>
	            												<td class="fc-event-container">
	            													<a class="fc-day-grid-event fc-event fc-start fc-end fc-draggable" href="http://google.com/" style="background-color:#3c8dbc;border-color:#3c8dbc">
	            														<div class="fc-content">
	            															<span class="fc-time">12a</span> 
	            															<span class="fc-title">Click for Google</span>
	            														</div>
	            													</a>
	            												</td>
	            												<td rowspan="2"></td>
	            												<td rowspan="2"></td>
	            												<td rowspan="2"></td>
	            											</tr>
	            											<tr>
	            												<td class="fc-event-container">
	            													<a class="fc-day-grid-event fc-event fc-start fc-end fc-draggable" style="background-color:#00c0ef;border-color:#00c0ef">
	            														<div class="fc-content">
	            															<span class="fc-time">12p</span> 
	            															<span class="fc-title">Lunch</span>
	            														</div>
	            													</a>
	            												</td>
	            												<td class="fc-event-container">
	            													<a class="fc-day-grid-event fc-event fc-start fc-end fc-draggable" style="background-color:#00a65a;border-color:#00a65a">
	            														<div class="fc-content">
	            															<span class="fc-time">7p</span> 
	            															<span class="fc-title">Birthday Party</span>
	            														</div>
	            													</a>
	            												</td>
	            											</tr>
	            										</tbody>
	            									</table>
	            								</div>
	            							</div>
	            							<div class="fc-row fc-week fc-widget-content" style="height: 77px;">
	            								<div class="fc-bg">
	            									<table>
	            										<tbody>
	            											<tr>
	            												<td class="fc-day fc-widget-content fc-sun fc-other-month fc-future" data-date="2016-10-02"></td>
	            												<td class="fc-day fc-widget-content fc-mon fc-other-month fc-future" data-date="2016-10-03"></td>
	            												<td class="fc-day fc-widget-content fc-tue fc-other-month fc-future" data-date="2016-10-04"></td>
	            												<td class="fc-day fc-widget-content fc-wed fc-other-month fc-future" data-date="2016-10-05"></td>
	            												<td class="fc-day fc-widget-content fc-thu fc-other-month fc-future" data-date="2016-10-06"></td>
	            												<td class="fc-day fc-widget-content fc-fri fc-other-month fc-future" data-date="2016-10-07"></td>
	            												<td class="fc-day fc-widget-content fc-sat fc-other-month fc-future" data-date="2016-10-08"></td>
	            											</tr>
	            										</tbody>
	            									</table>
	            								</div>
	            								<div class="fc-content-skeleton">
	            									<table>
	            										<thead>
	            											<tr>
	            												<td class="fc-day-number fc-sun fc-other-month fc-future" data-date="2016-10-02">2</td>
	            												<td class="fc-day-number fc-mon fc-other-month fc-future" data-date="2016-10-03">3</td>
	            												<td class="fc-day-number fc-tue fc-other-month fc-future" data-date="2016-10-04">4</td>
	            												<td class="fc-day-number fc-wed fc-other-month fc-future" data-date="2016-10-05">5</td>
	            												<td class="fc-day-number fc-thu fc-other-month fc-future" data-date="2016-10-06">6</td>
	            												<td class="fc-day-number fc-fri fc-other-month fc-future" data-date="2016-10-07">7</td>
	            												<td class="fc-day-number fc-sat fc-other-month fc-future" data-date="2016-10-08">8</td>
	            											</tr>
	            										</thead>
	            										<tbody>
	            											<tr>
	            												<td></td>
	            												<td></td>
	            												<td></td>
	            												<td></td>
	            												<td></td>
	            												<td></td>
	            												<td></td>
	            											</tr>
	            										</tbody>
	            									</table>
	            								</div>
	            							</div>
	            						</div>
	            					</div>
	            				</td>
	            			</tr>
	            		</tbody>
	            	</table>
	          	</div>
	            	</div>
		        	</div>
		      	</div>
	         <!-- /.box-body -->
	        </div>
		</div>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Your Page Content Here -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript::;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript::;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                  <span class="label label-danger pull-right">70%</span>
                </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- AdminLTE App -->
<!-- � necess�rio deixar no final da p�gina! -->
<script src="AdminLTE/dist/js/app.min.js"></script>
<script src="AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js"></script>
</body>
</html>