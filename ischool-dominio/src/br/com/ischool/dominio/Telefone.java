package br.com.ischool.dominio;

public class Telefone extends EntidadeDominio {

	private String numero;
	private DDD ddd;
	private Tipo tipo;
	private Operadora operadora;
	private Aluno aluno;
	
	public Aluno getAluno() {
		return aluno;
	}
	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public DDD getDdd() {
		return ddd;
	}
	public void setDdd(DDD ddd) {
		this.ddd = ddd;
	}
	public Tipo getTipo() {
		return tipo;
	}
	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}
	public Operadora getOperadora() {
		return operadora;
	}
	public void setOperadora(Operadora operadora) {
		this.operadora = operadora;
	}
	
	
}
