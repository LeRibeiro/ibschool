package br.com.ischool.dominio;

public class Tipo extends EntidadeDominio {

	private String descricao;
	private Integer tamanhoMax;

	public Integer getTamanhoMax() {
		return tamanhoMax;
	}

	public void setTamanhoMax(Integer tamanhoMax) {
		this.tamanhoMax = tamanhoMax;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
}
