package br.com.ischool.dominio;

public class Operadora extends EntidadeDominio {
	
	private String descricao;
	private Integer numero;
	
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Integer getNumero() {
		return numero;
	}
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	
	

}
