package br.com.ischool.dominio;

public class Aluno extends Pessoa {

	private Integer codMatricula;

	public Integer getCodMatricula() {
		return codMatricula;
	}

	public void setCodMatricula(Integer codMatricula) {
		this.codMatricula = codMatricula;
	}
	
}
