package br.com.ischool.core.fachada.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.ischool.core.aplicacao.Resultado;
import br.com.ischool.core.dao.IDAO;
import br.com.ischool.core.dao.impl.AlunoDAO;
import br.com.ischool.core.dao.impl.EnderecoDAO;
import br.com.ischool.core.dao.impl.TelefoneDAO;
import br.com.ischool.core.fachada.IFachada;
import br.com.ischool.core.strategy.IStrategy;
import br.com.ischool.core.strategy.impl.ComplementarDtCadastro;
import br.com.ischool.core.strategy.impl.ValidadorCpf;
import br.com.ischool.core.strategy.impl.ValidadorDadosObrigatoriosAluno;
import br.com.ischool.dominio.Aluno;
import br.com.ischool.dominio.Endereco;
import br.com.ischool.dominio.EntidadeDominio;
import br.com.ischool.dominio.Telefone;


public class Fachada implements IFachada {

	/** 
	 * Mapa de DAOS, ser� indexado pelo nome da entidade 
	 * O valor � uma inst�ncia do DAO para uma dada entidade; 
	 */
	private Map<String, IDAO> daos;
	
	/**
	 * Mapa para conter as regras de neg�cio de todas opera��es por entidade;
	 * O valor � um mapa que de regras de neg�cio indexado pela opera��o
	 */
	private Map<String, Map<String, List<IStrategy>>> rns;
	
	private Resultado resultado;
	
	
	public Fachada(){
		/* Instanciando o Map de DAOS */
		daos = new HashMap<String, IDAO>();
		/* Instanciando o Map de Regras de Neg�cio */
		rns = new HashMap<String, Map<String, List<IStrategy>>>();
		
		/* Criando inst�ncias dos DAOs a serem utilizados*/
		AlunoDAO aluDAO = new AlunoDAO();
		EnderecoDAO endDAO = new EnderecoDAO();
		TelefoneDAO telDAO = new TelefoneDAO();
		
		/* Adicionando cada dao no MAP indexando pelo nome da classe */
		daos.put(Aluno.class.getName(), aluDAO);
		daos.put(Endereco.class.getName(), endDAO);
		daos.put(Telefone.class.getName(), telDAO);
		
		
		
		/* Criando inst�ncias de regras de neg�cio a serem utilizados*/		
		ValidadorDadosObrigatoriosAluno vrDadosObrigatoriosAluno = new ValidadorDadosObrigatoriosAluno();
		ComplementarDtCadastro cDtCadastro = new ComplementarDtCadastro();
		ValidadorCpf vCpf = new ValidadorCpf();
		
		
		/* Criando uma lista para conter as regras de neg�cio de fornencedor
		 * quando a opera��o for salvar
		 */
		List<IStrategy> rnsSalvarAluno = new ArrayList<IStrategy>();
		/* Adicionando as regras a serem utilizadas na opera��o salvar do Aluno*/
		rnsSalvarAluno.add(vrDadosObrigatoriosAluno);
		rnsSalvarAluno.add(cDtCadastro);

		List<IStrategy> rnsAlterarAluno = new ArrayList<IStrategy>();
		/* Adicionando as regras a serem utilizadas na opera��o alterar do produto */
		rnsAlterarAluno.add(vCpf);
		
		
		
		/* Cria o mapa que poder� conter todas as listas de regras de neg�cio espec�fica 
		 * por opera��o  do fornecedor
		 */
		Map<String, List<IStrategy>> rnsAluno = new HashMap<String, List<IStrategy>>();
		/*
		 * Adiciona a listra de regras na opera��o salvar no mapa do aluno (lista criada na linha 70)
		 */
		rnsAluno.put("SALVAR", rnsSalvarAluno);	
		rnsAluno.put("ALTERAR", rnsAlterarAluno);
		
		/* Adiciona o mapa(criado na linha 79) com as regras indexadas pelas opera��es no mapa geral indexado 
		 * pelo nome da entidade
		 */
		rns.put(Aluno.class.getName(), rnsAluno);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		/* Criando uma lista para conter as regras de neg�cio de produto
		 * quando a opera��o for salvar

		
		/* Criando uma lista para conter as regras de neg�cio de produto
		 * quando a opera��o for alterar
		 */
		
	}
	
	
	@Override
	public Resultado salvar(EntidadeDominio entidade) {
		resultado = new Resultado();
		String nmClasse = entidade.getClass().getName();	
		
		String msg = executarRegras(entidade, "SALVAR");
		
		
		if(msg == null){
			IDAO dao = daos.get(nmClasse);
			try {
				dao.salvar(entidade);
				List<EntidadeDominio> entidades = new ArrayList<EntidadeDominio>();
				entidades.add(entidade);
				resultado.setEntidades(entidades);
			} catch (SQLException e) {
				e.printStackTrace();
				resultado.setMsg("N�o foi poss�vel realizar o registro!");
			}
		}else{
			resultado.setMsg(msg);
		}
		
		return resultado;
	}

	@Override
	public Resultado alterar(EntidadeDominio entidade) {
		resultado = new Resultado();
		String nmClasse = entidade.getClass().getName();	
		
		String msg = executarRegras(entidade, "ALTERAR");
	
		if(msg == null){
			IDAO dao = daos.get(nmClasse);
			try {
				dao.alterar(entidade);
				List<EntidadeDominio> entidades = new ArrayList<EntidadeDominio>();
				entidades.add(entidade);
				resultado.setEntidades(entidades);
			} catch (SQLException e) {
				e.printStackTrace();
				resultado.setMsg("N�o foi poss�vel realizar o registro!");
				
			}
		}else{
			resultado.setMsg(msg);
		}
		return resultado;
	}

	@Override
	public Resultado excluir(EntidadeDominio entidade) {
		resultado = new Resultado();
		String nmClasse = entidade.getClass().getName();	
		
		String msg = executarRegras(entidade, "CONSULTAR");
		
		
		if(msg == null){
			IDAO dao = daos.get(nmClasse);
			try {
				dao.excluir(entidade);
				List<EntidadeDominio> entidades = new ArrayList<EntidadeDominio>();
				entidades.add(entidade);
				resultado.setEntidades(entidades);
			} catch (SQLException e) {
				e.printStackTrace();
				resultado.setMsg("N�o foi poss�vel realizar o registro!");
			}
		}else{
			resultado.setMsg(msg);
		}
		return resultado;
	}

	@Override
	public Resultado consultar(EntidadeDominio entidade) {
		resultado = new Resultado();
		String nmClasse = entidade.getClass().getName();	
		
		String msg = executarRegras(entidade, "EXCLUIR");
		
		
		if(msg == null){
			IDAO dao = daos.get(nmClasse);
			try {
				resultado.setEntidades(dao.consultar(entidade));
			} catch (SQLException e) {
				e.printStackTrace();
				resultado.setMsg("N�o foi poss�vel realizar a consulta!");
			}
		}else{
			resultado.setMsg(msg);
		}
		return resultado;
	}
	
	@Override
	public Resultado visualizar(EntidadeDominio entidade) {
		resultado = new Resultado();
		resultado.setEntidades(new ArrayList<EntidadeDominio>(1));
		resultado.getEntidades().add(entidade);		
		return resultado;
	}

	
	private String executarRegras(EntidadeDominio entidade, String operacao){
		String nmClasse = entidade.getClass().getName();		
		StringBuilder msg = new StringBuilder();
		
		Map<String, List<IStrategy>> regrasOperacao = rns.get(nmClasse);
		
		if(regrasOperacao != null){
			List<IStrategy> regras = regrasOperacao.get(operacao);
			
			if(regras != null){
				for(IStrategy s: regras){			
					String m = s.processar(entidade);			
					if(m != null){
						msg.append(m);
						msg.append("\n");
					}			
				}	
			}			
		}
		if(msg.length()>0)
			return msg.toString();
		else
			return null;
	}
}
