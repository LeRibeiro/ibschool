package br.com.ischool.core.strategy.impl;

import java.util.List;

import br.com.ischool.core.strategy.IStrategy;
import br.com.ischool.dominio.Aluno;
import br.com.ischool.dominio.EntidadeDominio;
import br.com.ischool.dominio.Telefone;

public class ValidadorDadosObrigatoriosAluno implements IStrategy{

	@Override
	public String processar(EntidadeDominio entidade) {
		
		if(entidade instanceof Aluno){
			Aluno aluno = (Aluno)entidade;
//			List<Telefone> telefones = aluno.getTelefones();
			String nome = aluno.getNome();
//			
//			for(Telefone t: telefones){
//				
//				String numero = t.getNumero();
//				String operadora = t.getOperadora().getDescricao();
//				String tipo = t.getTipo().getDescricao();
//				
//				if(numero == null || operadora == null || tipo == null){
//					return "Numero, Operadora ou Tipo s�o de preenchimento obrigat�rio!!";
//				}
//				if(tipo.trim().equals("") || operadora.trim().equals("") || numero.trim().equals("")){
//					return "Numero, Operadora ou Tipo s�o de preenchimento obrigat�rio!!";
//				}
//				if(numero.length() != 9){
//					return "???????????????????????????????????????????????????????????????????????????????????????????????????";
//				}
//				
//			}
			
			if(nome == null || nome.trim().equals("")){
				return "Nome � de preenchimento obrigat�rio!";
			}			
		}else{
			return "Deve ser registrado um aluno!";
		}
		
		return null;
	}

}
