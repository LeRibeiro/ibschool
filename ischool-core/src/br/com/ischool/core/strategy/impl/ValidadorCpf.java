package br.com.ischool.core.strategy.impl;

import br.com.ischool.core.strategy.IStrategy;
import br.com.ischool.dominio.Aluno;
import br.com.ischool.dominio.EntidadeDominio;

public class ValidadorCpf implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		
		if(entidade instanceof Aluno){
			Aluno c = (Aluno)entidade;
			
			if(c.getCpf().length() < 9){
				return "CPF deve conter 14 digitos!";
			}
		}else{
			return "CPF n�o pode ser v�lidado, pois entidade n�o � um cliente!";
		}
		return null;
	}
}
