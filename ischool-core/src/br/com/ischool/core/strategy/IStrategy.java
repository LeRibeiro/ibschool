package br.com.ischool.core.strategy;

import br.com.ischool.dominio.EntidadeDominio;


public interface IStrategy 
{

	public String processar(EntidadeDominio entidade);
	
}
