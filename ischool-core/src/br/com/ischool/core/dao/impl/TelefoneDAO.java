package br.com.ischool.core.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.com.ischool.dominio.DDD;
import br.com.ischool.dominio.EntidadeDominio;
import br.com.ischool.dominio.Operadora;
import br.com.ischool.dominio.Telefone;
import br.com.ischool.dominio.Tipo;

public class TelefoneDAO extends AbstractJdbcDAO {

	public TelefoneDAO(String table, String idTable){
		super("tb_telefone", "id_telefone");
	}
	
	public TelefoneDAO(Connection cx) {
		super(cx, "tb_telefone", "id_telefone");
		// TODO Auto-generated constructor stub
	}
	
	public TelefoneDAO() {
		super("tb_telefone", "id_telefone");
	}

	@Override
	public void salvar(EntidadeDominio entidade) throws SQLException {
		if(connection == null){
			openConnection();
		}
		PreparedStatement pst = null;
		Telefone tel = (Telefone) entidade;
		StringBuilder sql = new StringBuilder();
		
		sql.append("INSERT INTO tb_telefone(numero, ddd, tipo, operadora_desc, operadora_num, id_alu ) ");
		sql.append(" VALUES (?,?,?,?,?,?)");
		
		try{
			connection.setAutoCommit(false);
						
			pst = connection.prepareStatement(sql.toString(), 
					Statement.RETURN_GENERATED_KEYS);
			
			pst.setString(1, tel.getNumero());
			pst.setInt(2, tel.getDdd().getNumero());
			pst.setString(3, tel.getTipo().getDescricao());
			pst.setString(4, tel.getOperadora().getDescricao());
			pst.setInt(5, tel.getOperadora().getNumero());
			pst.setInt(6, tel.getAluno().getId());
			pst.executeUpdate();		
					
			ResultSet rs = pst.getGeneratedKeys();
			int idTel = 0;
			if(rs.next())
				idTel = rs.getInt(1);
			tel.setId(idTel);
			
			connection.commit();					
			
		}catch (Exception e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();	
		}finally{
			if(ctrlTransaction){
				try {
					pst.close();
					if(ctrlTransaction)
						connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void alterar(EntidadeDominio entidade) throws SQLException {

		if(connection == null){
			openConnection();
		}
		PreparedStatement pst = null;
		Telefone tel = (Telefone) entidade;  
		
		
		try{
			
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE tb_telefone SET numero=?, ddd=?, tipo=?, operadora_desc=?, operadora_num=?, id_alu=? ) ");
			sql.append(" WHERE id_telefone=?");
			
		}catch (Exception e) {
			// TODO: handle exception
		}finally {
			try {
				
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
	}

	@Override
	public List<EntidadeDominio> consultar (EntidadeDominio entidade) throws SQLException {


		PreparedStatement pst = null;
		Telefone tel = (Telefone) entidade;
		StringBuilder sql = new StringBuilder();
		List<EntidadeDominio> tels = new ArrayList<EntidadeDominio>();		

		try {
			openConnection();
			sql.append("SELECT * FROM tb_telefone WHERE ");
			sql.append("id_alu=?");	
								
			pst = connection.prepareStatement(sql.toString());
			pst.setInt(1, tel.getAluno().getId());
			
			ResultSet rs = pst.executeQuery();			
			
			while(rs.next()){

				tel = new Telefone();
				DDD ddd = new DDD();
				tel.setDdd(ddd);
				Tipo tipo = new Tipo();
				tel.setTipo(tipo);
				Operadora op = new Operadora();
				tel.setOperadora(op);
				
				tel.setId(rs.getInt("id_telefone"));
				tel.getDdd().setNumero(rs.getInt("ddd"));
				tel.setNumero(rs.getString("numero"));
				tel.getTipo().setDescricao(rs.getString("tipo"));
				tel.getOperadora().setNumero(rs.getInt("operadora_num"));
				tel.getOperadora().setDescricao(rs.getString("operadora_desc"));
				
				tels.add(tel);
				
			}
			return tels;
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}		

}
