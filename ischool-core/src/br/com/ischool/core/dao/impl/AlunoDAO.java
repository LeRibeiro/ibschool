
package br.com.ischool.core.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import br.com.ischool.dominio.Aluno;
import br.com.ischool.dominio.Endereco;
import br.com.ischool.dominio.EntidadeDominio;
import br.com.ischool.dominio.Telefone;

public class AlunoDAO extends AbstractJdbcDAO {

	public AlunoDAO() {
		super("tb_aluno", "id_aluno"); // nome da coluna e da tabela no banco!
		// TODO Auto-generated constructor stub
	}

	@Override
	public void salvar(EntidadeDominio entidade) throws SQLException {
		openConnection();
		PreparedStatement pst = null;
		Aluno aluno = (Aluno) entidade;

		try {
			connection.setAutoCommit(false);			
			TelefoneDAO telDAO = new TelefoneDAO();
			telDAO.connection = connection;
			telDAO.ctrlTransaction = false;
			
			EnderecoDAO endDAO = new EnderecoDAO();
			endDAO.connection = connection;
			endDAO.ctrlTransaction = false;
			endDAO.salvar(aluno.getEndereco());
			
			
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO tb_aluno(dt_cadastro, nome, dt_nascimento, cpf, rg, id_end, email ");
			sql.append(") VALUES (?,?,?,?,?,?,?)");

			pst = connection.prepareStatement(sql.toString(), 
					Statement.RETURN_GENERATED_KEYS);

			
			Timestamp timeCad = new Timestamp(aluno.getDtCadastro().getTime());
			pst.setTimestamp(1, timeCad);
			pst.setString(2, aluno.getNome());
			Timestamp timeNas = new Timestamp(aluno.getDtNascimento().getTime());
			pst.setTimestamp(3, timeNas);
			pst.setString(4, aluno.getCpf());
			pst.setString(5, aluno.getRg());
			pst.setInt(6, aluno.getEndereco().getId());
			pst.setString(7, aluno.getEmail());
			pst.executeUpdate();
						
			ResultSet rs = pst.getGeneratedKeys();
			
			int id = 0;
			if(rs.next())
				id = rs.getInt(1);
			aluno.setId(id);
						
			connection.commit();
			
			Telefone t;
			for (int i = 0; i < aluno.getTelefones().size(); i++) {
				t = aluno.getTelefones().get(i);
				t.setAluno(aluno);
				telDAO.salvar(t);
			}
			
			} catch (SQLException e) {
				try {
					connection.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				e.printStackTrace();
			} finally {
				try {
					if(pst != null)
						pst.close();
					connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void alterar(EntidadeDominio entidade) throws SQLException {
		openConnection();
		PreparedStatement pst = null;
		Aluno aluno = (Aluno) entidade;

		try {
			connection.setAutoCommit(false);

			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE tb_aluno SET nome=?, dt_nasc=? ");
			sql.append("WHERE id_aluno=?");

			pst = connection.prepareStatement(sql.toString());
			pst.setString(1, aluno.getNome());
//			pst.setString(2, aluno.getDtNascimento());
			Timestamp timeNas = new Timestamp(aluno.getDtNascimento().getTime());
			pst.setTimestamp(3, timeNas);
			pst.executeUpdate();
			connection.commit();
			
			ResultSet rs = pst.getGeneratedKeys();
			int id=0;
			if(rs.next())
				id = rs.getInt(1);
			aluno.setId(id);
			
			
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
		
			PreparedStatement pst = null;
			openConnection();
			
			Aluno aluno = (Aluno) entidade;
			String sql = null;
			
		
		try {
			
			if(aluno.getNome() == null) {
				aluno.setNome("");
			}
			
			if(aluno.getId() == null && aluno.getNome().equals("")){
				sql = "SELECT * FROM tb_aluno";
			}else if(aluno.getId() != null && aluno.getNome().equals("")){
				sql = "SELECT * FROM tb_aluno WHERE id_aluno=?";
			}else if(aluno.getId() == null && !aluno.getNome().equals("")){
				sql = "SELECT * FROM tb_aluno WHERE nome like ?";
			}
			
			pst = connection.prepareStatement(sql);
			
			if(aluno.getId() != null && aluno.getNome().equals("")){
				pst.setInt(1, aluno.getId());
			}else if(aluno.getId() == null && !aluno.getNome().equals("")){
				pst.setString(1, "%"+aluno.getNome()+"%");			
			}
			

			// ResultSet � uma Tabela!
			ResultSet rs = pst.executeQuery();
			List<EntidadeDominio> alunos = new ArrayList<EntidadeDominio>();
			while (rs.next()) {
				Aluno a = new Aluno();
				
				/* Consultando o Aluno */
				
				a.setId(rs.getInt("id_aluno"));
				a.setNome(rs.getString("nome"));
				a.setDtNascimento(rs.getDate("dt_nascimento"));
				a.setCpf(rs.getString("cpf"));
				a.setRg(rs.getString("rg"));
				a.setEmail(rs.getString("email"));		
				a.setDtCadastro(rs.getDate("dt_cadastro"));
				Endereco end = new Endereco();
				end.setId(rs.getInt("id_end"));
				a.setEndereco(end);
				getEndereco(a);
	
				Telefone tel = new Telefone();
				tel.setAluno(a);
				
				a.setTelefones((List) getTelefone(tel));
				
				alunos.add(a);
			}
			return alunos;
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception ex){
			ex.printStackTrace();
		} finally  {
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	private void getEndereco(Aluno aluno){
		
		/* Consultando o Endere�o */
		EnderecoDAO endDAO = new EnderecoDAO();
		List<EntidadeDominio> ends = endDAO.consultar(aluno.getEndereco());
		aluno.setEndereco((Endereco) ends.get(0));
		
	}
	
	
	private List<EntidadeDominio> getTelefone(Telefone telefone){
		
		/* Consultando o Telefone */
		TelefoneDAO telDAO = new TelefoneDAO();
		List<EntidadeDominio> tels = null;
		try {
			tels = telDAO.consultar(telefone);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tels;
		
	}
		
}






