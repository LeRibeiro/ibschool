
package br.com.ischool.core.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.ischool.dominio.Cidade;
import br.com.ischool.dominio.Endereco;
import br.com.ischool.dominio.EntidadeDominio;
import br.com.ischool.dominio.Estado;
import br.com.ischool.dominio.Pais;

public class EnderecoDAO extends AbstractJdbcDAO {
	
	
	protected EnderecoDAO(String table, String idTable) {
		super("tb_endereco", "id_endereco");	
	}
	
	public EnderecoDAO(Connection cx){
		super(cx, "tb_endereco", "id_endereco");
	}
	
	public EnderecoDAO(){
		super("tb_endereco", "id_endereco");			
	}
	
	public void salvar(EntidadeDominio entidade) {
		if(connection == null){
			openConnection();
		}
		PreparedStatement pst = null;
		Endereco end = (Endereco) entidade;
		StringBuilder sql = new StringBuilder();
		
		sql.append("INSERT INTO tb_endereco(cidade, estado, ");
		sql.append("logradouro, numero, cep, dt_cadastro, complemento, bairro, pais ) ");
		sql.append(" VALUES (?,?,?,?,?,?,?,?,?)");	
		try {
			connection.setAutoCommit(false);
			
			pst = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			
			pst.setString(1, end.getCidade().getNome());
			pst.setString(2, end.getCidade().getEstado().getNome());
			pst.setString(3, end.getLogradouro());
			pst.setInt(4, end.getNumero());
			pst.setString(5, end.getCep());	
			Timestamp timeCadEnd = new Timestamp(new Date().getTime());
			pst.setTimestamp(6, timeCadEnd);
			pst.setString(7, end.getComplemento());
			pst.setString(8, end.getBairro());
			pst.setString(9, end.getCidade().getEstado().getPais().getNome());
			pst.executeUpdate();		
					
			ResultSet rs = pst.getGeneratedKeys();
			int idEnd=0;
			if(rs.next())
				idEnd = rs.getInt(1);
			end.setId(idEnd);
			
			connection.commit();					
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();	
		}finally{
			if(ctrlTransaction){
				try {
					pst.close();
					if(ctrlTransaction)
						connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}			
		}
	}

	
	@Override
	public void alterar(EntidadeDominio entidade) {
		if(connection == null){
			openConnection();
		}
		PreparedStatement pst = null;
		Endereco end = (Endereco) entidade;
		StringBuilder sql = new StringBuilder();
		
		
		try {
			
			sql.append("UPDATE tb_endereco SET cidade=?, estado=?, ");
			sql.append("rua=?, numero=?, cep=?, complemento=?, bairro=?, pais=? ");
			sql.append("WHERE id_endereco=?");	
						
			connection.setAutoCommit(false);
								
			pst = connection.prepareStatement(sql.toString());
			
			pst.setString(1, end.getCidade().getNome());
			pst.setString(2, end.getCidade().getEstado().getNome());
			pst.setString(3, end.getLogradouro());
			pst.setInt(4, end.getNumero());
			pst.setString(5, end.getCep());	
			pst.setString(6, end.getComplemento());
			pst.setString(7, end.getBairro());
			pst.setString(8, end.getCidade().getEstado().getPais().getNome());
			pst.setInt(9, end.getId());
			
			pst.executeUpdate();		
			connection.commit();
			
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();	
		}finally{
			if(ctrlTransaction){
				try {
					pst.close();
					if(ctrlTransaction)
						connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}			
		}
	}

	/** 
	 * TODO Descri��o do M�todo
	 * @param entidade
	 * @return
	 * @see fai.dao.IDAO#consulta(fai.domain.EntidadeDominio)
	 */
	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) {
		
		PreparedStatement pst = null;
		Endereco end = (Endereco) entidade;
		StringBuilder sql = new StringBuilder();
		List<EntidadeDominio> ends = new ArrayList<EntidadeDominio>();		

		try {
			openConnection();
			sql.append("SELECT * FROM tb_endereco WHERE ");
			sql.append("id_endereco=?");	
								
			pst = connection.prepareStatement(sql.toString());
			pst.setInt(1, end.getId());
			
			ResultSet rs = pst.executeQuery();			
			
			while(rs.next()){

				end = new Endereco();
				
				end.setId(rs.getInt("id_endereco"));
				end.setCidade(new Cidade());
				end.getCidade().setEstado(new Estado());
				end.getCidade().getEstado().setPais(new Pais());
				end.getCidade().setNome(rs.getString("cidade"));
				end.getCidade().getEstado().setNome(rs.getString("estado"));
				end.getCidade().getEstado().getPais().setNome(rs.getString("pais"));
				end.setLogradouro(rs.getString("logradouro"));
				end.setNumero(rs.getInt("numero"));
				end.setCep(rs.getString("cep"));	
				end.setComplemento(rs.getString("complemento"));
				end.setBairro(rs.getString("bairro"));
				
				ends.add(end);
			}
			return ends;
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
