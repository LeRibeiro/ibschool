package br.com.ischool.core.teste;

import br.com.ischool.core.dao.impl.EnderecoDAO;
import br.com.ischool.dominio.Endereco;

public class TesteEnderecoDAO {

	public static void main(String args[]){
		EnderecoDAO endDAO = new EnderecoDAO();
		
		Endereco end = new Endereco();
		end.setId(1);
		
		endDAO.consultar(end);
		
	}
	
}
