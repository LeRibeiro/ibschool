package br.com.ischool.core.teste;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.ischool.core.aplicacao.Resultado;
import br.com.ischool.core.fachada.impl.Fachada;
import br.com.ischool.dominio.Aluno;
import br.com.ischool.dominio.Cidade;
import br.com.ischool.dominio.DDD;
import br.com.ischool.dominio.Endereco;
import br.com.ischool.dominio.Estado;
import br.com.ischool.dominio.Operadora;
import br.com.ischool.dominio.Pais;
import br.com.ischool.dominio.Telefone;
import br.com.ischool.dominio.Tipo;

public class Teste {

	public static void main(String[] args) {
				
		Aluno aluno = new Aluno();
		aluno.setNome("John");
		aluno.setDtCadastro(new Date());
		aluno.setDtNascimento(new Date());
		aluno.setCpf("222.999.777-08");
		aluno.setRg("40.333.999-08");
		aluno.setEmail("john@hotmail.com");
		aluno.setEndereco(new Endereco());
		
		Telefone telefone = new Telefone();		
		DDD ddd = new DDD();
		ddd.setNumero(Integer.parseInt("11"));
		Operadora op = new Operadora();
		op.setDescricao("Vivo");
		op.setNumero(15);
		Tipo tipo = new Tipo();
		tipo.setDescricao("Celular");
		telefone.setNumero("4799-2233");
		telefone.setDdd(ddd);
		telefone.setOperadora(op);
		telefone.setTipo(tipo);
		
		List<Telefone> tels = new ArrayList<Telefone>();
		tels.add(telefone);
		
		aluno.setTelefones(tels);
		
		Endereco endereco = new Endereco();
		endereco.setLogradouro("Rua Sixteen");
		endereco.setNumero(413);
		endereco.setBairro("Centro");
		endereco.setCep("08777-321");
		endereco.setComplemento("apto 222");
		Cidade cid = new Cidade();
		cid.setNome("Cleveland");
		Estado estado = new Estado();
		Pais pais = new Pais();
		pais.setNome("USA");
		estado.setNome("Ohio");
		estado.setPais(pais);
		cid.setEstado(estado);
		endereco.setCidade(cid);
		
		endereco.setDtCadastro(new Date());
		aluno.setEndereco(endereco);
		
		
		// TESTANDO A FACHADA //
		Fachada f = new Fachada();
		Resultado resultado = f.salvar(aluno);
		
		
		// TESTANDO O DAO // - cadastrar
//		AlunoDAO aluDAO = new AlunoDAO();
//		try {
//			aluDAO.salvar(aluno);
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
				
	}

}
