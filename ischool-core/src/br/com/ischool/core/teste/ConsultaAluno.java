package br.com.ischool.core.teste;

import java.sql.SQLException;

import br.com.ischool.core.dao.impl.AlunoDAO;
import br.com.ischool.dominio.Aluno;
import br.com.ischool.dominio.EntidadeDominio;
import br.com.ischool.dominio.Telefone;

public class ConsultaAluno {

	public static void main(String[] args) throws SQLException {
		
		AlunoDAO alunoDAO = new AlunoDAO();
		Aluno aluno = new Aluno();
		aluno.setId(3);
		for (EntidadeDominio entDom : alunoDAO.consultar(aluno)) {
			Aluno alu = (Aluno) entDom;
			System.out.println("Nome: "+alu.getNome());
			System.out.println("Endereco: "+alu.getEndereco().getLogradouro());
			
			for(Telefone tels : alu.getTelefones()){
				System.out.println("Telefone: "+tels.getNumero());
				System.out.println("DDD :" + tels.getDdd().getNumero());
			}
		}
	}
}
